#ifndef __LINUX_I2C_H__
#define __LINUX_I2C_H__

// This part has been added in order to achieve C-C++ interoperability.
#ifdef __cplusplus
extern "C" {
#endif

uint8_t _i2c_init(int i2c, int dev_addr);

uint8_t _i2c_close();

uint8_t _i2c_write(uint8_t *ptr, int16_t len);

uint8_t _i2c_read(uint8_t *ptr, int16_t len);

// This part has been added in order to achieve C-C++ interoperability.
#ifdef __cplusplus
};
#endif

#endif
