//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#ifndef OLEDTHREADEXAMPLE_OLEDPAINTTHREAD_H
#define OLEDTHREADEXAMPLE_OLEDPAINTTHREAD_H

#include <thread>
#include <mutex>
#include <ctime>
#include <filesystem>
#include <vector>
#include <freetype/ftimage.h>
#include "OLEDDevice.h"

#define WIDTH   128
#define HEIGHT  64

class OLEDPaintThread {
private:
    int I2CDev;
    OLEDDevice* oled;
    std::mutex pictMutex;
    std::vector<unsigned char> picture;     // Used to store the image to be sent.
    std::thread *sendThread;
    std::filesystem::path fontFile;  // TODO: Getter and setter.
    bool running;
    uint8_t img[HEIGHT][WIDTH];

    void sendData();
    void drawDateTime();
    void freetypeDrawDateTime();
    void draw_bitmap(FT_Bitmap_ *bitmap, int x, int y);
    void packImage();

public:
    OLEDPaintThread(int, const std::filesystem::path &);
    ~OLEDPaintThread();
    std::vector<unsigned char> copyOfPicture();
    void stopRunning();
};


#endif //OLEDTHREADEXAMPLE_OLEDPAINTTHREAD_H
