//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#include "ProgramData.h"
#include "OLEDPaintThread.h"
#include <string>
#include <filesystem>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

#ifdef DEBUG
#include "debug_header.h"
#endif

namespace fs = std::filesystem;

using std::thread;
using std::vector;
using std::string;

ProgramData::ProgramData() {
    setupFonts();  // This should be executed prior to setupThreads.
    setupThreads();
    setupDateAndTime();
#ifdef DEBUG
    cout << "[I] " << fonts->size() << " fonts inserted." << endl;
#endif
}

ProgramData::~ProgramData() {
    //delete administratorThread;
    delete paintDateAndTimeThread;
    delete fonts;
}

//progData->fonts = Font::getFontsInCurDir();
//Font* curFont = fonts->begin();
// First, we create the threads. We need a thread that generates the graphical version of the date and time using a
// given typeface. Then, we need a thread that will accept an administrator connection. The administrator can change
// date and time, as well as the font face from a predefined list of fonts.
//paintDateAndTimeThread = createPaintThread();
//administratorThread = createAdministrator();

//paintDateAndTimeThread->run();
//administratorThread->run();
bool ProgramData::threadsAreRunning() {
    return false;
}

void ProgramData::showServerInfo() {

}

void ProgramData::showDateAndTime() {

}

void ProgramData::showFontInfo() {

}

void ProgramData::joinThreads() {
    // administratorThread->stopRunning();
    paintDateAndTimeThread->stopRunning();
    std::this_thread::sleep_for(5s);
}

void ProgramData::setupThreads() {
    paintDateAndTimeThread = new OLEDPaintThread(I2C, fonts->at(0));
    /* administratorThread = new AdministratorThread();
    paintDateAndTimeThread.run();
    administratorThread.run();*/
}

void ProgramData::setupFonts() {
    fonts = new vector<fs::path>();
    // Get fonts in current directory.
    for (const auto & entry : fs::directory_iterator(fs::current_path())) {
        if (entry.path().extension() == ".ttf") {
            fonts->insert(fonts->end(), entry.path());
#ifdef DEBUG
            cout << "-> Inserting " << fonts->at(fonts->size()-1) << " into font list." << endl;
#endif
        }
    }
}

void ProgramData::setupDateAndTime() {

}
