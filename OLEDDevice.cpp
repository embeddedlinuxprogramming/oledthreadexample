//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#include <cstring>
#include "OLEDDevice.h"
#include "ssd1306driver/ssd1306_linux/ssd1306.h"
#include "ssd1306driver/ssd1306_linux/linux_i2c.h"

#ifdef DEBUG
#include "debug_header.h"
#endif

void OLEDDevice::init() {
    // OLED initialization.
    if (ssd1306_init((uint8_t) I2CDev)) { // Error.
#ifdef DEBUG
        cout << "[E] Error: display not detected." << endl;
#endif
        initialized = false;
    }
    else {
#ifdef DEBUG
        cout << "[I] OLED device initialized on I2C" << I2CDev << "." << endl;
#endif
        initialized = true;
    }
}

void OLEDDevice::sendPicture(unsigned char *data) {
    if (initialized) {
        // Data is sent row by row, where every row has 128 bytes, and each byte represents
        // eight positions of the display (column-wise); hence 128 x 64.
        ssd1306_oled_clear_screen();
        uint8_t sendData[129];  // First byte will be the send command.
        // In order to prevent data flickering, an internal copy of the data
        // is used.
        uint8_t iData[1024];
        memcpy(iData, data, 1024);
        sendData[0] = SSD1306_DATA_CONTROL_BYTE;
        for (int row = 0; row < 8; ++row) {
            memcpy(sendData+sizeof(uint8_t), iData + 128*row, 128);
            _i2c_write(sendData, 129);
        }
    }
}

OLEDDevice::OLEDDevice(int i2c) : I2CDev(i2c) {
    init();
    if (initialized) {
        // Display size initialization.
        ssd1306_oled_default_config(64, 128);
        ssd1306_oled_clear_screen();
    }
}

bool OLEDDevice::isInitialized() const {
    return initialized;
}

OLEDDevice::~OLEDDevice() {
    if (initialized)
        ssd1306_end();
}
