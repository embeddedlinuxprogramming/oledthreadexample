//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#ifndef OLEDTHREADEXAMPLE_OLEDDEVICE_H
#define OLEDTHREADEXAMPLE_OLEDDEVICE_H


class OLEDDevice {
private:
    int I2CDev;
    bool initialized;

    void init();

public:
    OLEDDevice(int );
    ~OLEDDevice();
    void sendPicture(unsigned char *);
    bool isInitialized() const;
};


#endif //OLEDTHREADEXAMPLE_OLEDDEVICE_H
