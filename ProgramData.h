//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#ifndef OLEDTHREADEXAMPLE_PROGRAMDATA_H
#define OLEDTHREADEXAMPLE_PROGRAMDATA_H

#include "OLEDPaintThread.h"
#include <vector>
#include <ctime>
#include <filesystem>

#define I2C 2   // Should be the I2C device in which the display is connected.

class ProgramData {
private:
    OLEDPaintThread *paintDateAndTimeThread;
    // AdministrationThread *administratorThread;
    std::vector<std::filesystem::path> *fonts;
    tm curDateAndTime;
    // Private methods and functions.
    void setupThreads();
    void setupFonts();
    void setupDateAndTime();

public:
    ProgramData();
    ~ProgramData();

    bool threadsAreRunning();
    void showServerInfo();
    void showDateAndTime();
    void showFontInfo();
    void joinThreads();

};


#endif //OLEDTHREADEXAMPLE_PROGRAMDATA_H
