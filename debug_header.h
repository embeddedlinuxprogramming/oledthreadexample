//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#ifndef OLEDTHREADEXAMPLE_DEBUG_HEADER_H
#define OLEDTHREADEXAMPLE_DEBUG_HEADER_H

#include <iostream>

using std::cout;
using std::endl;

#endif //OLEDTHREADEXAMPLE_DEBUG_HEADER_H
