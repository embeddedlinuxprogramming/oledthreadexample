//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#include <cstring>
#include "OLEDPaintThread.h"
#include <chrono>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <strstream>
#include <cmath>

using namespace std::chrono_literals;

#ifdef DEBUG
#include "debug_header.h"
#endif

void OLEDPaintThread::sendData() {
    unsigned char buffer[1024];
    while (running) {
        drawDateTime();
        if (pictMutex.try_lock()) {
            memcpy(buffer, picture.data(), 1024);
            pictMutex.unlock();
            // TODO: send data to i2c
            if (oled->isInitialized())
                oled->sendPicture(picture.data());
        }
        std::this_thread::sleep_for(1s);
    }
}

void OLEDPaintThread::drawDateTime() {
    unsigned char buffer[1024];
    // TODO: Picture drawing code.
    freetypeDrawDateTime();
    pictMutex.lock();
    memcpy(picture.data(), buffer, 1024);
    pictMutex.unlock();
}

std::vector<unsigned char> OLEDPaintThread::copyOfPicture()  {
    pictMutex.lock();
    auto retval = picture;
    pictMutex.unlock();
    return retval;
}

OLEDPaintThread::OLEDPaintThread(int i2c, const std::filesystem::path& font) : I2CDev(i2c) {
    picture.resize(1024);
    running = true;
    sendThread = new std::thread([this]() { sendData(); });
    oled = new OLEDDevice(I2CDev);
    fontFile = font;
}

OLEDPaintThread::~OLEDPaintThread() {
    // TODO.
    sendThread->join();
    delete sendThread;
    delete oled;
}

void OLEDPaintThread::stopRunning() {
    running = false;
}

// Based upon the freetype examples (https://freetype.org/freetype2/docs/tutorial/example1.c).
void OLEDPaintThread::freetypeDrawDateTime() {
    auto dateTime = std::chrono::system_clock::now();
    std::time_t DT = std::chrono::system_clock::to_time_t(dateTime);
#ifdef DEBUG
    //cout << "[I] Current date and time: " << DT.year << endl;
#endif
    FT_Library    library;
    FT_Face       face;

    FT_GlyphSlot  slot;
    FT_Matrix     matrix;                 /* transformation matrix */
    FT_Vector     pen;                    /* untransformed origin  */
    FT_Error      error;
    char text[] = "30/08/23\n\r12:59:59";
    auto num_chars = strlen(text);
    auto angle= 0.0;
    error = FT_Init_FreeType( &library );              /* initialize library */
    /* error handling omitted */
    error = FT_New_Face( library, fontFile.c_str(), 0, &face ); /* create face object */
    /* error handling omitted */

    /* use 50pt at 100dpi */
    error = FT_Set_Char_Size( face, 16 * 64, 0,
                              100, 0 );                /* set character size */
    /* error handling omitted */

    /* cmap selection omitted;                                        */
    /* for simplicity we assume that the font contains a Unicode cmap */

    slot = face->glyph;
    /* set up matrix */
    matrix.xx = (FT_Fixed)( cos( angle ) * 0x10000L );
    matrix.xy = (FT_Fixed)(-sin( angle ) * 0x10000L );
    matrix.yx = (FT_Fixed)( sin( angle ) * 0x10000L );
    matrix.yy = (FT_Fixed)( cos( angle ) * 0x10000L );

    pen.x = 2;
    pen.y = ( HEIGHT - 16 ) * 64;

    for (int n = 0; n < num_chars; n++ ) {
        // set transformation
        FT_Set_Transform(face, &matrix, &pen);
        // load glyph image into the slot (erase previous one)
        error = FT_Load_Char(face, text[n], FT_LOAD_RENDER);
        if (error)
            continue;                 // ignore errors

        // now, draw to our target surface (convert position)
        draw_bitmap(&slot->bitmap, slot->bitmap_left, HEIGHT - slot->bitmap_top);
    }

    // Picture generation.
}

// Based upon the freetype examples (https://freetype.org/freetype2/docs/tutorial/example1.c).
void OLEDPaintThread::draw_bitmap(FT_Bitmap* bitmap, FT_Int x, FT_Int y) {
    FT_Int  i, j, p, q;
    FT_Int  x_max = x + bitmap->width;
    FT_Int  y_max = y + bitmap->rows;
    // for simplicity, we assume that `bitmap->pixel_mode'
    // is `FT_PIXEL_MODE_GRAY' (i.e., not a bitmap font)
    for ( i = x, p = 0; i < x_max; i++, p++ ) {
        for ( j = y, q = 0; j < y_max; j++, q++ ) {
            if ( i < 0      || j < 0       ||
                 i >= WIDTH || j >= HEIGHT )
                continue;
            img[j][i] |= bitmap->buffer[q * bitmap->width + p];
        }
    }
}

// Based upon the freetype examples (https://freetype.org/freetype2/docs/tutorial/example1.c).
void OLEDPaintThread::packImage() {
   int  i, j, bitpack, row;
   uint8_t tmp;
   for ( i = 0; i < HEIGHT>>3; i++ ) {
       for ( j = 0; j < WIDTH; j++ ) {
           picture.at(i*128 + j) = 0;
           for (bitpack = 0; bitpack<8; bitpack++) {
               row = (i<<3)+bitpack;
               picture.at(i*128 + j) |= (img[row][j]>128)<<bitpack;  // Packs 8 bits in one byte.
           }
       }
   }
}
