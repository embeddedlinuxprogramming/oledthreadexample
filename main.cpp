//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
//

#include <iostream>
#include "ProgramData.h"
#include <thread>
#ifdef DEBUG
#include "debug_header.h"
#endif

using namespace std;

// Global variables and constants.
ProgramData *progData;

//
int main(int argc, char* argv[]) {
    // Program setup.
    progData = new ProgramData();
    // "Infinite" loop.
    while (progData->threadsAreRunning()) {
        progData->showServerInfo();
        progData->showDateAndTime();
        progData->showFontInfo();
        cout << endl << endl;
        this_thread::sleep_for(5s); // 5 seconds.
    }
    cout << "Waiting for remaining processes to end..." << endl;
    progData->joinThreads();
    delete progData;
    auto dateTime = std::chrono::system_clock::now();
    std::time_t DT = std::chrono::system_clock::to_time_t(dateTime);
    std::string strDT = std::ctime(&DT);
    cout << "[I] Program ended at " << strDT.c_str() << endl;
    return 0;
}


